from flask_restful import Resource, reqparse

class YPStudiesCtrl(Resource):
    # /v1/api/ypstudies
    # will return an array of users (JSON objects)
    def get(self):
        import json
        import cx_Oracle as cx
        compound = 'ain'
        ora_host = 'chbsux0425.eu.novartis.net'
        ora_port = 1521
        ora_sid  = 'PRO280'
        ora_usr  = os.environ.get('YP_USR')
        ora_pwd  = os.environ.get('YP_PWD')
        dsn = cx.makedsn(ora_host,ora_port,ora_sid)
        con = cx.connect(user=ora_usr, password=ora_pwd, dsn=dsn)
        cur = con.cursor()
        cur.execute("SELECT /*+ PARALLEL(8) */ DISTINCT STUDY_NAME FROM dialmst.VW_YP_FOLDER_ALL WHERE STUDY_NAME LIKE '%"+compound.upper()+"%' AND LENGTH(STUDY_NAME) > 8 AND STUDY_NAME NOT LIKE 'lost+found' ORDER BY STUDY_NAME")
        rows = cur.fetchall()
        studies_list = []
        for row in rows:
            study = ''.join(row)
            studies_list.append(study)
        j = json.dumps(studies_list)
        con.close()
        return j
